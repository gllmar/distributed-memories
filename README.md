# Distributed Memories

[[_TOC_]]


## en

[**Distributed Memories**](http://sabrinaratte.com/Distributed-Memories-2021) is an installation on the interweaving between electronic and human memory, the influence of an outside observer on the interpretation of our memories and on the creative process in dialogue with technology. [Guillaume Arseneault](http://gllmar.ca) and [Sabrina Ratté](http://sabrinaratte.com) offer an interactive installation in which the sequence of images and sounds (created by [Roger Tellier Craig](https://rogertelliercraig.com/)) is influenced by the presence of visitors. This space distributes multiple perspectives on over 10 years of formal studies carried out by [Sabrina Ratté](http://sabrinaratte.com).

## fr

[**Distributed Memories**](http://sabrinaratte.com/Distributed-Memories-2021) est une installation sur l’entrelacement entre la mémoire électronique et humaine, l’influence d’un observateur extérieur sur l’interprétation de nos souvenirs et sur le processus créatif en dialogue avec la technologie. [Guillaume Arseneault](http://gllm.ca) et [Sabrina Ratté](http://sabrinaratte.com) proposent une installation interactive où l’enchaînement des images et des sons (créés par [Roger Tellier Craig](https://rogertelliercraig.com/)) est influencé par la présence des visiteurs. Cet espace distribue multiples perspectives sur 10 ans d’études formelles réalisées par [Sabrina Ratté](http://sabrinaratte.com/).

---

[![distributed memories](medias/distributed_memories_thumb_1250.jpg)](medias/distributed_memories_thumb_5000.jpg)


## [documentation_technique](/%2E%2E/wikis/home)

